from pypinyin import pinyin, Style

def GetSentencePinyin(chiese_sentence):
    # 使用 phrase-pinyin-data 项目中 cc_cedict.txt 文件中的拼音数据优化结果
    from pypinyin_dict.phrase_pinyin_data import cc_cedict
    cc_cedict.load()
    # from pypinyin_dict.pinyin_data import kMardari
    # kxhc1983.load()
    pinyin_list = []
    str_sentence = "".join(chiese_sentence)
    pinyin_all_sentence = pinyin(str(str_sentence), Style.TONE3, heteronym=False, errors='default', strict=True)
    for pinyin_word in pinyin_all_sentence:
        pinyin_list.append(pinyin_word[0])
    # print("pinyin_list:", pinyin_list)
    return pinyin_list

label = ['武','松', '大','声', '喝','斥', '拿','酒', '来', '喝', '了', '十', '二','碗','博', '得', '众', '食', '客', '一', '阵', '喝', '彩',]
GetSentencePinyin(label)