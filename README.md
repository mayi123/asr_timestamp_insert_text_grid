将ASR引擎的时间戳进行对齐并生成TextGrid
=====
## ```generate_scp_linux.py``` 是批量生成音频路径的脚本
### 执行的命令为：```python generate_scp_linux.py```路径需要为音频所在路径
### 生成的文件路径为：```wav/wav.scp```
<br />

## ASR时间戳模型的客户端为：``` asr_timestamp_client.py```
### 执行的命令为：
### ```python asr_timestamp_client.py  --host 172.29.52.197 --port 10095 --mode offline --audio_in wav/wav.scp --thread_num 1 --output_dir result/ ```
### 生成两个文件：
### ```result/result.txt ``` 即ASR引擎返回的识别结果文件，用来进行对齐
### ```result/timestamp.json ``` 即ASR引擎返回的识别结果含时间戳的Json文件
<br />

## 与真值进行对齐的脚本：```label_alignment.py```  
### 执行的命令为：
### ```python label_alignment.py --char=1 --v=1 label/test_long_text result/result.txt > result/alignment.txt ```
### 生成两个文件：
### ```result/alignment.txt ``` 即对齐的结果文件
### ```result/alignment.json ``` 即对齐结果生成的Json文件
<br />

## 生成TextGrid的脚本：```generate_TextGrid.py```
### 执行的命令为：```python generate_TextGrid.py ```路径需修改为json所在路径

### 生成的文件为：
### ```TextGrid/```下的对应```audio_name.TextGrid``` 
### TextGrid文件的时间戳和音频持续时间以及识别结果及其拼音
### ```timestamp.json ```和```alignment.json ``` 提供

### 可执行：```python generate_TextGrid.py > timestamp_detail.txt```  来获取TextGrid中每个字对应的时间戳。
#### 注： 模型在一些情况下生成的时间戳个数低于识别到的字符个数
#### 这时候会在生成的TextGrid文件前加“#”区分，如#SPKR00021.TextGrid
#### 也会在```timestamp_detail.txt```中显示缺少的时间戳个数，如：SPKR00021.TextGrid已生成但缺少 1 个时间戳！