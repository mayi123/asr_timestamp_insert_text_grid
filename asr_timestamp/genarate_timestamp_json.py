# -*- encoding: utf-8 -*-
# -- coding: utf-8 --
import json
import os


def generate_timestamp_json(input_dir, output_dir):
    # 读取duration.txt文件
    with open(input_dir + "duration.txt", "r") as f:
        duration_data = {}
        for line in f:
            wav_name, duration = line.strip().split('  ')
            duration_data[wav_name] = float(duration)

    # 读取result.txt文件
    with open(input_dir + "result.txt", "r", encoding='utf-8') as f:
        result_data = {}
        for line in f:
            wav_name, result = line.strip().split('  ')
            result_data[wav_name] = result

    # 读取timestamp.txt文件
    with open(input_dir + "timestamp.txt", "r") as f:
        timestamp_data = {}
        for line in f:
            wav_name, time_stamps = line.strip().split('  ')
            timestamp_data[wav_name] = list(json.loads(time_stamps))

    # 合并三个字典
    merged_data = {}
    for wav_name in duration_data.keys():
        merged_data[wav_name] = {
            "rec": result_data[wav_name],
            "time_stamp": timestamp_data[wav_name],
            "duration": duration_data[wav_name]
        }

    ibest_rec_time_json = open(os.path.join(output_dir, "timestamp.json"), "a", encoding="utf-8")
    json.dump(merged_data, ibest_rec_time_json, ensure_ascii=False)


if __name__ == '__main__':
    generate_timestamp_json(r'result/', r'result/')
