import os
import re

# 设置音频文件所在目录
audio_dir = '/home00/dwenxu/asr_timestamp_insert_text_grid/asr/wer0-50andtext10-15-wav'
# 设置输出文件名
output_file = '/home00/dwenxu/asr_timestamp_insert_text_grid/asr/wav/wav.scp'


def natural_sort_key(s):
    return [
        int(text) if text.isdigit() else text.lower()
        for text in re.split(r'(\d+)', s)
    ]


def generate_scp(audio_dir, audio_files):
    # 获取音频文件列表，并排序
    audio_files = sorted(os.listdir(audio_dir), key=natural_sort_key)
    # 生成scp文件内容
    with open(output_file, 'w', encoding='utf-8') as f:
        for audio_file in audio_files:
            if audio_file.endswith('.wav'):
                # 获取不含文件格式的音频文件名
                file_name = os.path.splitext(audio_file)[0]
                # 拼接绝对路径
                absolute_path = os.path.join(audio_dir, audio_file).replace('\\', '/')
                # 将路径写入到scp文件中
                f.write(f'{file_name} {absolute_path}\n')

    print("SCP 文件已生成.")


if __name__ == "__main__":
    generate_scp(audio_dir, output_file)
