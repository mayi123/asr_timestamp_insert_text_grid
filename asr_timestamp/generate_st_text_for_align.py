'''
    生成用于生成 alignment.json 所需的text
        label/test_long_text

    这里的标签从csv文件中提取或者是从  声通/云之声的评测json文件中提取
'''

import os
import json

audio_path = r"E:\正式标注数据\已选数据\wer0-50andtext10-15"

# 指定要写入的TXT文件路径
txt_file_path = r'E:\正式标注数据\已选数据\textgrid生成所需文件'

if __name__ == '__main__':
    # 创建一个空列表来存储要写入的内容
    label_output_data = []
    # 遍历JSON文件夹
    label_file_path = os.path.join(txt_file_path, "label.txt")
    for root, dirs, files in os.walk(audio_path):
        for file in files:
            if file.endswith('.json'):
                json_file_path = os.path.join(root, file)
                # 提取音频ID（去除后缀）
                audio_id = os.path.splitext(file)[0]
                # 读取JSON文件并提取sample文本
                with open(json_file_path, 'r', encoding='utf-8') as json_file:
                    data = json.load(json_file)
                    sample_text = data.get('refText')

                # 添加audio_id和sample文本到列表
                label_output_data.append(f'{audio_id} {sample_text}')
                # result_output_data.append(f'{audio_id} {result_text}')
    print(f'共有{len(label_output_data)}条数据')

    # 一次性写入文件
    with open(label_file_path, 'w', encoding='utf-8') as txt_file:
        txt_file.write('\n'.join(label_output_data))
    print(f'Data has been written to {label_file_path}')

