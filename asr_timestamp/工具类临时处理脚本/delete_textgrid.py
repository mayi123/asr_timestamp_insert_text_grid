
import os


def delete_tg_in_dir(dir_path):
    # 获取文件夹下的所有文件名
    files = os.listdir(dir_path)
    # 遍历所有文件名
    counter = 0
    for file in files:
        # 检查文件扩展名是否为.TextGrid
        if file.endswith('.TextGrid'):
            # 构造完整的文件路径
            file_path = os.path.join(dir_path, file)
            # 删除.TextGrid文件
            os.remove(file_path)
            counter += 1
    print(f'已删除{dir_path}路径下的{counter}个textgrid文件')


if __name__ == '__main__':
    delete_tg_in_dir(r'E:\正式标注数据\已选数据\wer0-50andtext10-15-wav')