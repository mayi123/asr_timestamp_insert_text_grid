**Funasr识别和时间戳切分服务启动**

`docker run {container_name}`

`cd FunASR/runtime`
`nohup bash run_server.sh --download-model-dir /workspace/models --vad-dir damo/speech_fsmn_vad_zh-cn-16k-common-onnx --model-dir  damo/speech_paraformer-large-vad-punc_asr_nat-zh-cn-16k-common-vocab8404-onnx --punc-dir damo/punc_ct-transformer_cn-en-common-vocab471067-large-onnx --certfile 0 > log.out 2>&1 &`

docker退出容器但不停止运行： Ctrl+P+Q

##  1. 筛选出需要进行标注的音频文件

1.  ### 音频文件最好转换为 wav 形式

2.  ### 准备音频的参考文本标签 **label.txt** 格式如下：(识别结果文件 **result.txt** 相同格式 )

   ```txt
   000f82da26bb4e1e80d66c700b5cf6ad 我国发现过一头近四十吨重的鲸约十八米长一条舌头就有十几头大肥猪那么重
   00121c196f244eb68c92d45c885fa286 他买桔子水日益熟练起来情绪日益高涨最终成了一种狂热
   001d754b8131439a88afd90410df4186 虽然人生道路很长但关键之处往往只有几步
   004057eeeec149779f58848c177d1c4b 猎人抬头一看一只老鹰抓着一条小白蛇正从他头上方飞过他急忙搭箭开弓对准老鹰射去老鹰受了伤丢下小白蛇逃了
   004ede60947443b198e7bccaddcba418 世界神话传说中藏着无穷无尽的奥秘我们读得越多收获就越多会让我们对世界充满幻想与希望
   005d0c49f4d7414a941e7509bea3318f 两个人走马灯似地转了三四圈终于三抓两挠揪在了一起
   00619ae757b9454485ac1d501af276b8 表演藏戏的艺人们席地而唱不要幕布不要灯光不要道具只要一鼓一钹为其伴奏
   ```

执行脚本： **generate_st_text_for_align.py**

## 2. 生成 label.txt 和 timestamp.json

1. ### 数据上传到服务器文件夹   

​	例如：/wav

2. ### 生成对应的scp文件  

​	使用脚本  `generate_scp_linux.py`生成 **wav.scp**

3. ### 调用FunASR语音识别接口生成识别结果 **result.txt**、时间戳 **timestamp.json**

   调用FunASR 识别服务命令

   ```shell
   python asr_timestamp_client.py --host "10.109.118.139" --port 10096 --mode offline --audio_in wav/wav.scp --thread_num 10 --ssl 0 --output_dir result
   ```

该命令调用识别接口会生成如下文件：

* result.txt：识别结果

* timestamp.txt：识别结果及单字对应的时间戳

* duration.txt: 每个音频的时长

* timestamp.json：生成tg文件所需的时间戳json文件（若该文件没有自动生成，则需要手动执行 **asr_timestamp_client.generate_timestamp_json**方法生成）

   
## 3. 生成导入系统所需的textgrid文件

### 1.  调用与真值进行对齐的脚本：`label_alignment.py`

```shell
python label_alignment.py --char=1 --v=1 label/label.txt result/result.txt > result/alignment.txt
```

生成两个文件： 

​	**result/alignment.txt** ：即对齐的结果文件

​	**result/alignment.json** ： 即对齐结果生成的Json文件**(生成tg文件时使用这个文件)**

### 2. 调用生成TextGrid的脚本：```generate_TextGrid.py```

在其中修改一下**alignment.json** 和 **timestamp.json**这两个文件的位置即可



